/*****************************************************************************
 * Copyright (c) Minh Duc Cao, Monash Uni & UQ, All rights reserved.         *
 *                                                                           *
 * Redistribution and use in source and binary forms, with or without        *
 * modification, are permitted provided that the following conditions        *
 * are met:                                                                  * 
 *                                                                           *
 * 1. Redistributions of source code must retain the above copyright notice, *
 *    this list of conditions and the following disclaimer.                  *
 * 2. Redistributions in binary form must reproduce the above copyright      *
 *    notice, this list of conditions and the following disclaimer in the    *
 *    documentation and/or other materials provided with the distribution.   *
 * 3. Neither the names of the institutions nor the names of the contributors*
 *    may be used to endorse or promote products derived from this software  *
 *    without specific prior written permission.                             *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS   *
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, *
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR    *
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR         *
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,     *
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,       *
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR        *
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF    *
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING      *
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS        *
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.              *
 ****************************************************************************/

/**************************     REVISION HISTORY    **************************
 * 3 Jan 2015 - Minh Duc Cao: Created                                        
 *  
 ****************************************************************************/
package japsa.bio.hts.scaffold;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author minhduc&sonnguyen
 *
 */
public class ScaffoldGraphDFS extends ScaffoldGraph {
	/**
	 * @param sequenceFile
	 * @throws IOException
	 */
	public ScaffoldGraphDFS(String sequenceFile) throws IOException {
		super(sequenceFile);

	}
	
	public ScaffoldGraphDFS(String sequenceFile, String graphFile) throws IOException {
		super(sequenceFile);
		//TODO: implement fastg reader for SequenceReader to have pre-assembled bridges

	}
	
	/* 
	 * Arrange contigs
	 * 
	 */
	@Override
	public synchronized void connectBridges(){
		// Start scaffolding
		if(verbose) 
			System.out.println("Starting scaffolding.......");
		
		List<LengthIndex> list = new ArrayList<LengthIndex>();
		for(int i = 0; i<contigs.size(); i++)
			list.add(new LengthIndex(contigs.get(i).length(),i));
		Collections.sort(list);
		for(LengthIndex idx:list){
			int i = idx.index;
			//TODO: include also the case of circularized scaffold (for realtime self-correction)
			if (contigs.get(i).head !=i || scaffolds[i].size() < 1 || scaffolds[i].closeBridge != null)
				continue;
			//Now extend scaffold i				
			if(	isRepeat(scaffolds[i].element()) 
				|| scaffolds[i].element().length() < minContigLength){
				if(!scaffolds[i].element().isCircular)
					continue;
			}
			//1.a extend to the first
			boolean closed = false;
			/*****************************************************************/
			////////////////////////////First try to extend to the end/////////////////////////////
			if(verbose) 
				System.out.printf("Extending %d to the rear\n",i);
			closed = walk2(i,true);

			////////////////////////////Then extend to the front///////////////////////////////////

			if (!closed){
				if(verbose) 
					System.out.printf("Extending %d to the front\n",i);
				closed = walk2(i,false);
			}
			//scaffolds[i].trim();
			/*****************************************************************/
			if(verbose && scaffolds[i].size() > 1) 
				System.out.printf("Finally, scaffold %d size %d  and is %s\n",
					i,
					scaffolds[i].getLast().rightMost() - scaffolds[i].getFirst().leftMost(),
					closed?"circular":"linear");			
		}//for

	}

	
	/* 	To check the agreement of locations between 3 contigs: marker -> prevContig -> curContig
	 *	(also their corresponding scores?). 
	 * 	Purpose: avoid false positive alignments.
	 *	
	 */
	private ContigBridge checkHang(Contig marker, ContigBridge toPrev, ContigBridge toCurrent){
		Contig 	prevContig = toPrev.secondContig,
				curContig = toCurrent.secondContig;
		ScaffoldVector 	mark2Prev = toPrev.getTransVector(),
						mark2Cur = toCurrent.getTransVector();
		ScaffoldVector prevToCur = ScaffoldVector.composition(mark2Cur,ScaffoldVector.reverse(mark2Prev));
		if(verbose) 
			System.out.printf("\texamining %s to %s:\n ",prevContig.getName(),curContig.getName());
		for(ContigBridge brg:prevContig.bridges){
			if(brg.secondContig.getIndex() == curContig.getIndex()){
				if(brg.consistentWith(prevToCur)){
					if(verbose) 
						System.out.printf("=> consistent between bridge vector %s and estimated vector %s\n",brg.getTransVector(),prevToCur);
					return brg; 
				}
				else{
					if(verbose) 
						System.out.printf("=> inconsistent between bridge vector %s and estimated vector %s\n",brg.getTransVector(),prevToCur);
				}
			}

		}	
		
		return null;
	}
	
	/*
	 * Walking through markers while trying to fill the gap by repeat sequences simultaneously
	 */
	private boolean walk2(int i, boolean direction ){	
		Scaffold scaffold = scaffolds[i];					
		boolean extended = true;
		boolean closed = false;

		/*****************************************************************/
		while (extended && (!closed) && scaffold.size() > 0){
			Contig ctg = direction?scaffold.getLast():scaffold.getFirst();				
			if(verbose) {
				System.out.printf(" Last of scaffold %d extention is on contig %d (%s): ",i,ctg.getIndex(),ctg.getName());
				System.out.printf("iterating among %d bridges\n",ctg.bridges.size());
			}
			int ctgEnd = direction?ctg.rightMost():ctg.leftMost();
			 
			extended = false; //only continue the while loop if extension is on the move (line 122)
			int maxLink = ctg.bridges.size(),
				extendDir = 0,
				curStep = Integer.MAX_VALUE; //distance between singleton1 -> singleton2
			double	curScore = 0.0; //score between singleton1 -> singleton2
			ContigBridge stepBridge = null;
			
			ArrayList<Contig> extendableContig = new ArrayList<Contig>(maxLink);
			ArrayList<ContigBridge> extendableContigBridge = new ArrayList<ContigBridge>(maxLink);
			ArrayList<ScaffoldVector> extendableVector = new ArrayList<ScaffoldVector>(maxLink);
			ArrayList<Integer> distances = new ArrayList<Integer>(maxLink);
			Collections.sort(ctg.bridges);
			for (ContigBridge bridge:ctg.bridges){
				if (bridge.firstContig == bridge.secondContig) //2 identical markers ??!
					if(!bridge.firstContig.isCircular)
						continue;
				Contig nextContig = bridge.secondContig;					
				ScaffoldVector trans = bridge.getTransVector();
//				if (ctg == bridge.secondContig){
//					nextContig = bridge.firstContig;
//					trans = ScaffoldVector.reverse(trans);
//				}					
				if(verbose) 
					System.out.println("..." + nextContig.getName());
				ScaffoldVector trialTrans = ScaffoldVector.composition(trans, ctg.getVector());
				int newEnd = direction?nextContig.rightMost(trialTrans):nextContig.leftMost(trialTrans);				
				
				//see if the next contig would extend the scaffold to the right
				//only take one next singleton (with highest score possible sorted) as the marker for the next extension
				int distance = bridge.getTransVector().distance(bridge.firstContig, bridge.secondContig);
				if (direction?(newEnd > ctgEnd):(newEnd < ctgEnd)){	
					if(!isRepeat(nextContig) || (ctg.isCircular && ctg.getIndex() == nextContig.getIndex())){
						//check quality of the bridge connected 2 markers
						int aDir = 0;
						if(scaffolds[nextContig.head].size() > 1){
							aDir = extendDirection(ctg, bridge);
							if(aDir==0){
								if(verbose)
									System.out.println("No jump to "  + nextContig.getName());
								continue;
							}
						}
						if(distance > -maxRepeatLength && bridge.getConnections().size() >= minSupportReads && bridge.getScore() > curScore ){
//							if(verbose)
//								bridge.display();
							curStep = distance;
							curScore = bridge.getScore();
							stepBridge = bridge;
							extendDir = aDir;
						}else{
							if(verbose)
								System.out.printf("Cannot form unique bridge from %d to %d with %d connections and score %.2f\n", 
												ctg.getIndex(), nextContig.getIndex(), bridge.getConnections().size(), bridge.getScore());
							continue;
						}
					}
					
					if(verbose) 
						System.out.printf(" Might extend %d from %d(%d) to %d(%d) (%s direction) with score %f and distance %d\n"
										,i,ctg.index, ctgEnd, nextContig.index, newEnd, 
										(bridge.getTransVector().getDirection() > 0?"same":"opposite"), bridge.getScore(), distance);
					
					int j = 0;
					//looking for right position to have the list sorted
					for(j=0; j<distances.size(); j++)
						if(distances.get(j) > distance)
							break;

					distances.add(j,distance);
					extendableContig.add(j, nextContig);
					extendableContigBridge.add(j, bridge);
					extendableVector.add(j, trialTrans);
				}
				else if(verbose) 
						System.out.printf(" No extend %d from %d(%d) to %d(%d) with score %f and distance %d\n",i,ctg.index, ctgEnd, nextContig.index, newEnd,bridge.getScore(), distance);
				
			}//for	
			int noOfUniqueContig = 0; //reset to count how many singleton will be added now
			
			if(stepBridge==null){
				if(verbose) 
					System.out.printf(" Extension of Scaffold %d toward stopped at %d due to the lack of next marker!\n", i,ctg.index);
				return false;
			}
			int curEnd = ctgEnd;
			Contig prevContig = ctg;
			ContigBridge 	prevContigBridge = null;
			ScaffoldVector prevVector = new ScaffoldVector();
			for(int index = 0; index < extendableContig.size(); index++){
				if(distances.get(index) > curStep)
				continue;
				Contig curContig = extendableContig.get(index);
				ContigBridge curContigBridge = extendableContigBridge.get(index); //will be replaced by the bridge to prev contig later
				ScaffoldVector curVector = extendableVector.get(index);
				if(verbose) 
					System.out.println("Checking contig " + curContig.getName() + "...");
				if(	isRepeat(curContig) && !curContig.isCircular)
					if(checkHang(ctg, curContigBridge, stepBridge)==null)
						continue;
				prevVector = prevContig.getVector();
				boolean extendable = false;
				ScaffoldVector prevToCur = ScaffoldVector.composition(curVector,ScaffoldVector.reverse(prevVector));
				ContigBridge confirmedBridge;
				//TODO: if this happen with singleton -> chimeric happen (unique+repeat=contig) need to do smt...
				if(	isRepeat(curContig) &&
					(direction?(curContig.rightMost(curVector) < curEnd):(curContig.leftMost(curVector)) > curEnd)){
					if(verbose) 
						System.out.println(curContig.getName() + " is ignored because current end " + curEnd + 
										" cover the new end " + (direction?curContig.rightMost(curVector): curContig.leftMost(curVector)));
					continue;
				}
				else{	
					if(index >= 1 && prevContigBridge != null){
						confirmedBridge = checkHang(ctg, prevContigBridge, curContigBridge);
						if(confirmedBridge != null){
							prevContigBridge = curContigBridge;
							prevToCur =confirmedBridge.getTransVector();
							extendable = true;
						}
						else
							continue;
					}
					else{
						prevContigBridge = curContigBridge;
						confirmedBridge = curContigBridge;
						extendable = true;
					}
				}		
				if(extendable){
					// if extension is circularized
					if(curContig.getIndex() == (direction?scaffold.getFirst().getIndex():scaffold.getLast().getIndex())
						&& (!isRepeat(curContig) || curContig.isCircular)
						){
						if(verbose) 
							System.out.printf(" *****************SCAFFOLD %d CLOSED AFTER CONNECT %d ***********************\n", i,curContig.index);
						scaffold.setCloseBridge(direction?confirmedBridge:getReversedBridge(confirmedBridge));
						curContigBridge.setContigScores();
						return true;
					}
					
					if(isRepeat(curContig)){
						curContig.head = i; //must be here!
						
						curContig.isCircular = false; // tandem!
						curContig = curContig.clone();
					}else{
						//check to join 2 scaffolds and stop this round
						if (scaffolds[curContig.head].size() > 1){
							if(!joinScaffold(prevContig,confirmedBridge,extendDir)){
								System.out.printf(" Skip to connect contig %d of %d to contig %d of %d\n", ctg.index,i,curContig.index, curContig.head);
								continue;
							}			
							else{
								curContigBridge.setContigScores();
								noOfUniqueContig++;
								break;
							}

						}
						curContig.head = i; //must be here!
						noOfUniqueContig++;
						curContigBridge.setContigScores();
					}

					if(verbose) {						
						System.out.printf(" Extend %d from %d(%d) to %d(%d) with score %f: ",
							i,ctg.index, ctgEnd, curContig.index, curContig.rightMost(curVector), curContigBridge.getScore());						
						System.out.printf(" curContigBridge %d -> %d\n", confirmedBridge.firstContig.getIndex(), curContigBridge.secondContig.getIndex());
					}
					
					if(direction)
						scaffolds[i].addRear(curContig, confirmedBridge);
					else
						scaffolds[i].addFront(curContig, getReversedBridge(confirmedBridge));
					curContig.myVector = ScaffoldVector.composition(prevToCur,prevContig.getVector());//from the head contig
					curEnd = direction?curContig.rightMost(curVector):curContig.leftMost(curVector);
					extended = true; //scaffold extension is really on the move...									
					
					prevContig = curContig;
					
					if(verbose)
						scaffolds[i].view();

				}
				
				if(distances.get(index) == curStep)
					break;
			}//for
			if(noOfUniqueContig < 1){
				if(verbose) 
					System.out.printf(" Extension of Scaffold %d toward stopped at %d because next marker is not reachable!\n", i,ctg.index);
				scaffolds[i].trim();
				return false;
			}
			// TO THE NEXT UNIQUE CONTIG
		}//while
		return closed;
	}
	class LengthIndex implements Comparable<LengthIndex>{
		int length, index;
		public LengthIndex(int len, int index){
			this.length = len;
			this.index = index;
		}
		/* (non-Javadoc)
		 * @see java.lang.Comparable#compareTo(java.lang.Object)
		 */
		@Override
		public int compareTo(LengthIndex o) {
			return (int) (o.length - length);

		}	
	}
}
